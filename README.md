# Certification Linux Essentials (LPI) - Paquets CentOS

Bonjour et bienvenue dans ce scénario créé par **Jordan ASSOULINE** pour vous aider à comprendre comment fonctionnent les symboles " et ', ainsi que les caractères d'échappement.

Pour commencer, tapez la commande suivante sur votre terminal:

`sudo apt update && sudo apt install git -y && git clone https://gitlab.com/lpi3/linuxessentials/3.-symboles-et-caracteres-d-echappement.git`

<br/>

# Exercice 1

Dans cette première étape, nous allons chercher à comprendre le fonctionnement des "doubles quotes" dans la ligne de commande Bash.

## Comment fonctionnent les doubles quotes ?

Entre les caractères : " ", tous les caractères spéciaux perdent leur signification sauf :
- $ (dollar)
- \ (backslash)
- ` (backquote)

Par exemple la substitution de la variable **$USER** n’est pas affectée par les doubles quotes.

<br/>

1. Essayez de taper la commande `echo Je suis $USER`

   

2. Maintenant exécutez la commande suivante : `echo "Je suis $USER"`

<br/>

**Q1: Que peut-on en conclure ? (2 réponses vraies)**

[ ] Les deux commandes renvoient le même résultat

[ ] Les commandes n'ont pas du tout le même effet

[ ] La variable "$USER" est substituée par le nom de l'utilisateur même lorsqu'elle se trouve entre double quotes

[ ] La variable "$USER" n'est pas substituée par le nom de l'utilisateur lorsqu'elle se trouve entre double quotes

<details><summary> Montrer la solution </summary>
<p>
[*] Les deux commandes renvoient le même résultat
</p>
<p>
[*] La variable "$USER" est substituée par le nom de l'utilisateur même lorsqu'elle se trouve entre double quotes
</p>
</details>
<br/>

**Q2: Quelle est la valeur de "$USER" ?**

<details><summary> Montrer la solution </summary>
<p>
`echo $USER`{{execute}}
</p>
<p>
root
</p>
</details>
<br/>

# Exercice 2

Continuons à observer le comportement des doubles quotes.

<br/>

## Le caractère "espace"

Lorsqu'il est utilisé dans la ligne de commande, le caractère espace possède une signification de "séparateur d'arguments".

Vérifions cela.

La commande **touch** permet de créer de nouveaux fichiers vides.

3. Exécutez la commande `touch nouveau fichier`. 

<br/>

4. Puis grâce à la commande **ls**, vérifiez les fichiers présents dans le répertoire actuel : `ls -l`

<br/>

**Q1: Combien de fichiers sont présents dans le répertoire actuel ?**
<details><summary> Montrer la solution </summary>
<p>
Il y a 2 fichiers dans le répertoire actuel.
</p>
</details>
<br/>

## Utilisation des doubles quotes

Exécutez la commande suivante : `rm -Rf *`. Cela permet de supprimer le/les fichiers que nous avons créé précédemment.

<br/>

5. Maintenant exécutez de nouveau la commande de la partie précédente, mais en encadrant les arguments par les doubles quotes : `touch "nouveau fichier"`

<br/>

**Q2: Combien de fichiers sont présents dans le répertoire actuel ?**
<details><summary> Montrer la solution </summary>
<p>
Il n'y a plus qu'un fichier dans le répertoire actuel.
</p>
</details>
<br/>

# Exercice 3

Reprenons les mêmes exemples que précédemment, mais cette fois-ci en utilisant les "single quotes".

<br/>

## Comparaison avec l'utilisation des single quotes

6. Essayez la commande `echo Je suis $USER`

   <br/>

7. Maintenant, effectuez la même commande, mais en encadrant les arguments avec des "single quotes" : `echo 'Je suis $USER'`

   <br/>

**Q1: Que peut-on en conclure ? (2 réponses vraies)**

[ ] Les deux commandes renvoient le même résultat

[ ] Les commandes n'ont pas du tout le même effet

[ ] La variable "$USER" est substituée par le nom de l'utilisateur même lorsqu'elle se trouve entre les single quotes

[ ] La variable "$USER" n'est pas substituée par le nom de l'utilisateur lorsqu'elle se trouve entre les single quotes

<details><summary> Montrer la solution </summary>
<p>
[*] Les commandes n'ont pas du tout le même effet
</p>
<p>
[*] La variable "$USER" n'est pas substituée par le nom de l'utilisateur lorsqu'elle se trouve entre les single quotes
</p>
</details>
<br/>

# Exercice 4

On peut également utiliser les caractères d’échappement pour supprimer les significations particulières des caractères dans le langage Bash.

<br/>

## Utilisation du backslash

8. Essayez la commande `echo Je suis $USER`

   <br/>

9. Maintenant, effectuez la même commande, mais en ajoutant le caractère "\" avant le **$USER** : `echo Je suis \$USER`

   <br/>

**Q1: Que peut-on en conclure ? (2 réponses vraies)**

[ ] Les deux commandes renvoient le même résultat

[ ] Les commandes n'ont pas du tout le même effet

[ ] La variable "$USER" est substituée par le nom de l'utilisateur même lorsqu'elle se trouve précédée du "backslash"

[ ] La variable "$USER" n'est pas substituée par le nom de l'utilisateur lorsqu'elle se trouve entre précédée du "backslash"

<details><summary> Montrer la solution </summary>
<p>
[*] Les commandes n'ont pas du tout le même effet
</p>
<p>
[*] La variable "$USER" n'est pas substituée par le nom de l'utilisateur lorsqu'elle se trouve entre précédée du "backslash"
</p>
</details>
